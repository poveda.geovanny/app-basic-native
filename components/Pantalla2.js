import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { StyleSheet } from 'react-native';

// componente para re-utilizar vistas

import Stack1 from './Stack1';
import Stack2 from './Stack2';

// este el contenedor sobre el cual debo guardar
// los elementos de la vista.

const Stack = createStackNavigator();


const Pantalla2 = () => {

<Stack.Navigator options="false" >

    <Stack.Screen name="Stack1" component={Stack1} options={{ headerShown: false }}/>
    <Stack.Screen name="Stack2" component={Stack2} options={{ headerShown: false }}/>

</Stack.Navigator>





    
}




