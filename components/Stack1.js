import * as React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

const Stack1 = (props) => {
    return (
        <View style={styles.layout}>
            <Text style={styles.title}>Pantalla Stack 1</Text>
            <Button
                title="Adelante"
                onPress={() => props.navigation.navigate('Stack2')}
            />
        </View>
    );
};
