import React from 'react';
import { StyleSheet, Text, View, Button }
 from 'react-native';
import { useNavigation } from 
'@react-navigation/native';

import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { useNavigation } from '@react-navigation/native';


const Home = () => {

    // navigation a otro recurso 
    const navigation = useNavigation();

    return(
        // vista
        <View style={styles.container}>
            

            // un texto que imprime "HOME"
            <Text style={styles.title}>
                Home
            </Text>
            // Un boton con una accion.
            <Button
                title="Abrir modal"
                onPress={() => navigation.navigate('PantallaModal')}
            />
        </View>
    );


    const styles = StyleSheet.create({
        container: {
            flex: 1,
            justifyContent: 'center',
            padding: 8,
        },
        title: {
            margin: 24,
            fontSize: 18,
            fontWeight: 'bold',
            textAlign: 'center',
        }
    });

    
};



